<?php
session_start();
if($_SESSION['admin'] == 0){
    header('location:index.php');
}
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
         <?php
     if (isset($_SESSION['login'])) {
  echo "<a class='navbar-brand' href='admin.php'>".$_SESSION["login"]."</a> |";
  }
  ?>
         
       
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
    
                        
                        
                        <li class="message-footer">
                            <a href="#"></a>
                        </li>
                    </ul>
                </li>
                
<style>
img{
    max-width: 200px;
    height: 150px;
    margin-left: 5px;
}
.foto{
opacity: 0.4;
}
.foto:hover{
    opacity: 0.9;
    border:solid 1px black;
}
</style>
                
        
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
  <?php       
 include_once('header.php');
 ?>
</ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Dodaj zdjęcia
                        </h1>
<form action="" method="post" enctype="multipart/form-data">
<table class='table table-bordered'>
    <label for="pliki">Wybierz plik</label>
    <input type="file" name="pliki[]" multiple="multiple"><br>
    <input type="submit" name="wyslano" value="Wrzuć pliki...">
</form>

 <?php
include('db-connect.php');
   $z=0;
if(isset($_POST['wyslano'])){
    $max_files = 10; // maksymalna ilość plików
    $max_file_size = 40000000; // maksymalny rozmiar 1 pliku w bajtach
    $allowed = array('jpg','png'); // dozwolone rozszerzenia plików
    $folder = "upload/"; // folder, do którego mają być zapisywane pliki
    $images_id = rand(0,99999); // identyfikator wrzucanej grupy plików
 
    try{
        // SPRAWDZAMY, CZY ZOSTAŁ WYBRANY JAKIKOLWIEK PLIK
        if(!is_uploaded_file($_FILES['pliki']['tmp_name'][0])){
            throw new Exception("Musisz wybrać plik.");
        }
 
        // SPRAWDZAMY, ILE PLIKÓW ZOSTAŁO WYBRANYCH
        if(count($_FILES['pliki']['tmp_name'])>$max_files){
            throw new Exception("Możesz wrzucić maksymalnie ".$max_files." plików.");
        }
 
        // TWORZYMY PĘTLĘ, KTÓRA POZWOLI NAM "DOTRZEĆ" DO KAŻDEGO PLIKU PO KOLEI
        echo "<br><br><form action='admin_photo_hex.php' method='get'>";
        for($i=0;$i<count($_FILES['pliki']['tmp_name']);$i++){
 
            // SPRAWDZAMY, CZY NAZWA PLIKU ZAWIERA KROPKĘ, CO POZWOLI NAM POBRAĆ ROZSZERZENIE
            if(preg_match("/\./", $_FILES['pliki']['name'][$i])){
                $explode = explode(".", $_FILES['pliki']['name'][$i]);
                $ext = end($explode);
                // SPRAWDZAMY CZY ROZSZERZENIE WYSŁANEGO PLIKU ZNAJDUJE SIĘ W TABLICY DOZWOLONYCH ROZSZERZEŃ
              
            }
 
            // SPRAWDZAMY, CZY ROZMIAR PLIKU JEST ODPOWIEDNI
            if($_FILES['pliki']['size'][$i]>$max_file_size){
                throw new Exception("Plik <i>".$_FILES['pliki']['name'][$i]."</i> posiada zbyt duży rozmiar. Dopuszczalny maksymalny rozmiar pliku to ".($max_file_size/1048576)."MB");
            }
 
            // NADJEMY PLIKOWI NAZWĘ WG. WZORU: CZASWYSŁANIA_IDGRUPY_NRZDJĘCIA
            $file_name = (isset($ext)) ? time()."_".$images_id."_".$i.".".$ext : time()."_".$images_id."_".$i;
            // ZAPISUJEMY PLIK
            move_uploaded_file($_FILES['pliki']['tmp_name'][$i], $folder.$file_name);
            echo"<img src='./upload/$file_name' class='foto'>"; 
            $data = date("Y-m-d");
             $z++;
            if($z==1){
            $sql = $conn->query("INSERT INTO `photos`( `url`,`id_grupy`,`hex`,`data`) VALUES ('$file_name','$images_id','1','$data')");
        }else{
            $sql = $conn->query("INSERT INTO `photos`( `url`,`id_grupy`,`hex`,`data`) VALUES ('$file_name','$images_id','0','$data')");
      
        }
        }

            echo "</form><br><font color=\"green\">Pliki zostały wgrane!</font>";
                
    }
    // WYŚWIETLAMY ZŁAPANE WYJĄTKI (BŁĘDY)
    catch(Exception $e){
        die("<font color=\"red\">".$e->getMessage()."</font>");
    }
}
?> 
  <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Edytuj album
                        </h1>
Kliknij w okładkę albumu, aby edytować<br>
 <?php
include('db-connect.php');
   $sql = "SELECT * FROM photos WHERE hex=1 ORDER BY id DESC"; 
 $result= $conn->query($sql);
        while($row = mysqli_fetch_array($result)){                  
echo"<a class='hexIn' href='admin_gallery_edit.php?id=".$row['id_grupy']."'>
      <img src='upload/".$row['url']."' class='foto'/>
    </a>";
}
  $conn->close();
?>
                </div>
                <!-- /.row -->

                <!-- Flot Charts -->
                <div class="row">

                </div>
                <!-- /.row -->

                


            </div>


        </div>


    </div>

    

</body>

</html>
