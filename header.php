<!DOCTYPE html>
<html lang="pl">

<head>
        <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mateusz Rozkosz">

    <title>Wypożyczalnia w kratkę</title>

    <!-- Bootstrap Core CSS -->
    <link href="./css/bootstrap.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <!-- Theme CSS -->
    <link href="./css/style.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/responsiveslides.css">
    <!--FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Francois+One&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

    <link href='http://fonts.googleapis.com/css?family=Josefin+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>

  <script src="js/responsiveslides.min.js"></script>
  <link rel="stylesheet" type="text/css" href="./source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <script type="text/javascript" src="./source/jquery.fancybox.js?v=2.1.5"></script>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53233102-2', 'auto');
  ga('send', 'pageview');

$(document).ready(function(){
    $("#menu_mobile_open").click(function(){
      $('#menu_mobile').show(60);
      $('#menu_mobile_close').show(60);
      $('#menu_mobile_open').css("display", "none");
    });
});
$(document).ready(function(){
 $("#menu_mobile_close").click(function(){
      $('#menu_mobile').hide();
      $('#menu_mobile_close').hide(0);
       $('#menu_mobile_open').show(0);
    });
});
  </script>
   
    <div id="menu">
      <div id="logo"><a href="index.php"><img src="./img/logo.png" width="195px" height="58px"></a></div>
        <ul>
          <li><a href="cennik.php">Cennik</a></li>
          <li><a href="o_nas.php">O nas</a></li>
          <li><a href="kontakt.php">Kontakt</a></li>
        </ul>
        <span></span>
        <span></span>
        <span></span>
        <div id="menu_mobile_open"></div>
    </div>

    <div id="menu_mobile">
      <div id="menu_mobile_close"></div>
      <ul>
          <li><a href="cennik.php">Cennik</a></li>
          <li><a href="o_nas.php">O nas</a></li>
          <li><a href="kontakt.php">Kontakt</a></li>
        </ul>
    </div>
 
</head>
<body>