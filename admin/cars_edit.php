<?php
session_start();
  if($_SESSION['admin'] == 0){
    header('location:index.php');
}
  ?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
         <?php
if (isset($_SESSION['login'])) {
  echo "<a class='navbar-brand' href='admin.php'>".$_SESSION["login"]."</a> |";
  }
  ?>
         
       
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
    
                        
                        
                        <li class="message-footer">
                            <a href="#"></a>
                        </li>
                    </ul>
                </li>
                
<style>
img{
    max-width: 200px;
    height: 150px;
    margin-left: 5px;
}
.btn{
    margin-top: 10px;
    min-width:50px;
    height: 30px;
    background: white;
    box-shadow: 3px 3px 3px;
    border:1px solid;
}
input[type="radio"]:checked{
background: grey;
}
input.cena{
   width: 60px;
}
input[type="text"]{
    margin-top: 5px;
}
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}

.cc-selector-2 input:active +.drinkcard-cc, .cc-selector input:active +.drinkcard-cc{opacity: 0.9;}
.cc-selector-2 input:checked +.drinkcard-cc, .cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    min-width:50px;height:30px;
    background: #a1a1a1;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.8) grayscale(1) opacity(.8);
       -moz-filter: brightness(1.8) grayscale(1) opacity(.8);
            filter: brightness(1.8) grayscale(1) opacity(.8);
}
.drinkcard-cc:hover{
    -webkit-filter: brightness(1.2) grayscale(0.7) opacity(.9);
       -moz-filter: brightness(1.2) grayscale(0.7) opacity(.9);
            filter: brightness(1.2) grayscale(0.7) opacity(.9);
}

/* Extras */
a:visited{color:#888}
a{color:black;text-decoration:none;}
p{margin-bottom:.3em;}
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}


</style>
                
        
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
  <?php       
 include_once('header.php');
 ?>
</ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                <form action="cars_update.php" method="post">
                    <?php
include('db-connect.php');
   $_SESSION["id"] = $_GET['id'];
   $id=$_GET['id'];      
   $sql = "SELECT * FROM samochody WHERE id='$id'"; 
 $result=$conn->query($sql);
        while($row = mysqli_fetch_array($result)){
$_SESSION['id_auta']=$row['id_auta']; 
$_SESSION['id_grupy']=$row['id_grupy'];                    
 $id_auta=$row['id_auta'];                          
 $id_grupy=$row['id_grupy'];
 $marka=$row['marka'];
 $model=$row['model'];
echo '<input type="text" name="marka" placeholder="Marka" value="'.$row['marka'].'">
                          <input type="text" name="model" placeholder="Model" value="'.$row['model'].'">
                        </h1>
                     <table class="tg">
  <tr>
    <td class="tg-yw4l">Rok produkcji:</td>
    <td class="tg-yw4l"><input type="data" name="rok_produkcji" placeholder="Rok produkcji (2001)" value="'.$row['rok_produkcji'].'"></td>
    <td class="tg-yw4l">Silnik:</td>
     <td class="tg-yw4l"><input type="text" name="silnik" placeholder="Silnik (1.2 TDI)" value="'.$row['silnik'].'"></td>
  </tr>
  <tr>
   <td class="tg-yw4l">Moc:</td>
   <td class="tg-yw4l"><input type="text" name="moc" placeholder="Moc" value="'.$row['moc'].'"></td>
    <td class="tg-yw4l">Liczba drzwi:</td>
    <td class="tg-yw4l"><input type="text" name="miejsca" placeholder="Liczba drzwi" value="'.$row['miejsca'].'"></td>
  </tr>
   <tr>   
   <td class="tg-yw4l">Spalanie:</td>             
    <td class="tg-yw4l"><input type="text" name="zuzycie_paliwa" placeholder="Spalanie" value="'.$row['zuzycie_paliwa'].'"></td>
      <td class="tg-yw4l">Zbiornik paliwa:</td>
    <td class="tg-yw4l"><input type="text" name="bak" placeholder="Zbiornik paliwa (l)" value="'.$row['bak'].'"></td>
  </tr>
  <tr>   
   <td class="tg-yw4l">Tapicerka:</td>  
    <td class="tg-yw4l"><input type="text" name="tapicerka" placeholder="Tapicerka" value="'.$row['tapicerka'].'"></td>
   <td class="tg-yw4l">Wyposażenie:</td>
    <td class="tg-yw4l"><textarea name="inne" placeholder="'.$row['inne'].'" rows="2" cols="50">'.$row['inne'].'</textarea></td>          
 
  </tr>
 
         </table>


                        <h4>Wybierz rodzaj paliwa:</h4>
                     <div class="cc-selector">';
                     echo '<input type="radio" name="paliwo" value="'.$row['paliwo'].'" id="inne" checked="checked" />
                    <label class="drinkcard-cc visa btn" for="inne">'.$row['paliwo'].'</label></input>';

                   echo" <input type='radio' name='paliwo' value='Benzyna' id='benzyna' />
                    <label class='drinkcard-cc visa btn' for='benzyna'>Benzyna </label></input>
                    <input type='radio' name='paliwo' value='diesel' id='diesel' />
                    <label class='drinkcard-cc visa btn' for='diesel'>Diesel </label></input>
                    <input type='radio' name='paliwo' value='Benzyna + LPG' id='benzynaLPG' />
                    <label class='drinkcard-cc visa btn' for='benzynaLPG'>Benzyna + LPG </label></input>
                </div>";
                echo' <h4>Wybierz skrzyni biegów:</h4>
                     <div class="cc-selector">
                     <input type="radio" name="gear" value="'.$row['gear'].'" id="gear" checked="checked" />
                    <label class="drinkcard-cc visa btn" for="gear">'.$row['gear'].'</label></input>';
                    echo"<input type='radio' name='gear' value='Automatyczna' id='automatyczna' />
                    <label class='drinkcard-cc visa btn' for='automatyczna'>Automatyczna </label></input>   
                     <input type='radio' name='gear' value='Manualna' id='manualna' />
                    <label class='drinkcard-cc visa btn' for='manualna'>Manualna </label></input>
                </div>";
                echo ' <h4>Typ pojazdu:</h4>
                     <div class="cc-selector">';
                      echo '<input type="radio" name="typ" value="'.$row['typ'].'" id="typ" checked="checked" />
                    <label class="drinkcard-cc visa btn" for="typ">'.$row['typ'].'</label></input>';


                   echo' <input  type="radio" value="hatchback" name="typ" id="body_type_hatchback">
                    <label class="drinkcard-cc visa btn" for="body_type_hatchback">Hatchback</label>
                    <input  type="radio" value="sedan" name="typ" id="body_type_sedan">
                    <label class="drinkcard-cc visa btn" for="body_type_sedan">Sedan/Limuzyna</label>
                    <input  type="radio" value="station-wagon" name="typ" id="body_type_station-wagon">
                    <label class="drinkcard-cc visa btn" for="body_type_station-wagon">Kombi</label>
                    <input  type="radio" value="mpv" name="typ" id="body_type_mpv">
                    <label class="drinkcard-cc visa btn" for="body_type_mpv">Minivan</label>
                    <input  type="radio" value="pick-up" name="typ" id="body_type_pick-up">
                    <label  class="drinkcard-cc visa btn" for="body_type_pick-up">Pick-up</label><br>
                    <input  type="radio" value="roadster" name="typ" id="body_type_roadster">
                    <label class="drinkcard-cc visa btn" for="body_type_roadster">Kabriolet</label>
                    <input  type="radio" value="coupe" name="typ" id="body_type_coupe">
                    <label class="drinkcard-cc visa btn" for="body_type_coupe">Sportowy/Coupe</label>
                    <input  type="radio" value="suv" name="typ" id="body_type_suv">
                    <label class="drinkcard-cc visa btn" for="body_type_suv">SUV</label>
                    <input  type="radio" value="off-road" name="typ" id="body_type_off-road">
                    <label class="drinkcard-cc visa btn" for="body_type_off-road">Terenowy</label>
                    <input  type="radio" value="van" name="typ" id="body_type_van">
                    <label class="drinkcard-cc visa btn" for="body_type_van">Van (minibus)</label><br>
                </div>
                    <h4>Kolor:</h4>
                     <input type="text" name="kolor" placeholder="kolor" value="'.$row['kolor'].'">';

                        $sql_cena = "SELECT * FROM cennik WHERE id_auta='$id_auta'"; 
 $result_cena=$conn->query($sql_cena);
        while($row_cena = mysqli_fetch_array($result_cena)){

echo'<h4>Uzupełnij cennik:</h4>
                    <table class="tg">
  <tr>
    <td class="tg-yw4l">1 - 7 DNI</td>
    <td class="tg-yw4l">8 - 14 DNI</td>
    <td class="tg-yw4l">15 - 30 DNI</td>
    <td class="tg-yw4l">MIESIĄC</td>
    <td class="tg-yw4l">KAUCJA</td>
  </tr>
  <tr>
    <td class="tg-yw4l"> <input class="cena" type="text" name="cena_promocyjna" placeholder="70" value="'.$row_cena['cena_promocyjna'].'" > zł<br></td>
  <td class="tg-yw4l"> <input class="cena" type="text" name="cena_1" placeholder="70" value="'.$row_cena['cena_1'].'" > zł<br></td>
 <td class="tg-yw4l"> <input class="cena" type="text" name="cena_2" placeholder="70" value="'.$row_cena['cena_2'].'"> zł<br></td>
 <td class="tg-yw4l"> <input class="cena" type="text" name="cena_3" placeholder="70" value="'.$row_cena['cena_3'].'"> zł<br></td>
 <td class="tg-yw4l"> <input class="cena" type="text" name="cena_4" placeholder="70" value="'.$row_cena['cena_4'].'"> zł<br></td>
 
  </tr>
  
</table>';
                           
}
}
  $conn->close();
?>

<br>
<br>
<br>
<input type='submit' name='zapisz' value='Zapisz'></form>
 <br><br>
                </div>
                <!-- /.row -->

                <!-- Flot Charts -->
                <div class="row">

                </div>
                <!-- /.row -->

                


            </div>


        </div>


    </div>

    

</body>

</html>
