<?php
session_start();
  if($_SESSION['admin'] == 0){
    header('location:index.php');
}
  ?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
         <?php
     if (isset($_SESSION['login'])) {
  echo "<a class='navbar-brand' href='admin.php'>".$_SESSION["login"]."</a> |";
  }
  ?>
         
       
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
    
                        
                        
                        <li class="message-footer">
                            <a href="#"></a>
                        </li>
                    </ul>
                </li>
                
<style>
img{
    max-width: 200px;
    height: 150px;
    margin-left: 5px;
    border: 1px solid;
}
.foto{
opacity: 0.4;
}
.foto:hover{
    opacity: 0.9;
    border:solid 1px black;
}
td{
    float: left;
}
.img{
    height: 250px;
    max-width: 220px;
    float: left;
}
</style>
                
        
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
  <?php       
 include_once('header.php');
 ?>
</ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Edytuj informacje o pojeździe 
                        </h1>
                
 <?php
include('db-connect.php'); 
   $sql = "SELECT * FROM samochody ORDER BY `id` DESC"; 
 $result=$conn->query($sql);
        while($row = mysqli_fetch_array($result)){
                           
 $id_grupy=$row['id_grupy'];
 $marka=$row['marka'];
 $model=$row['model'];
echo"<div class='img'><a class='hexIn' href='cars_edit.php?id=".$row['id']."'>";
echo $marka." ".$model." | Edytuj</a> | <a href='cars_usun.php?id=".$row['id']."'>Usuń</a>";
$sql_1 = ("SELECT * FROM photos WHERE id_grupy='$id_grupy' and hex='1'"); 
 $resultt=$conn->query($sql_1);
        while($roww = mysqli_fetch_array($resultt)){
    echo "<a class='hexIn' href='cars_edit.php?id=".$row['id']."'><img src='upload/".$roww['url']."' class='foto'/></a></div>";    
}
}
  $conn->close();
?>

                </div>
                <!-- /.row -->

                <!-- Flot Charts -->
                <div class="row">

                </div>
                <!-- /.row -->

                


            </div>


        </div>


    </div>

    

</body>

</html>
