<?php       
 include_once('header.php');
 include('db-connect.php');
 ?>
 <script type="text/javascript">
    $(document).ready(function() {
      /*
       *  Simple image gallery. Uses default settings
       */

      $('.fancybox').fancybox();

      /*
       *  Different effects
       */

      // Change title type, overlay closing speed
      $(".fancybox-effects-a").fancybox({
        helpers: {
          title : {
            type : 'outside'
          },
          overlay : {
            speedOut : 0
          }
        }
      });

      // Disable opening and closing animations, change title type
      $(".fancybox-effects-b").fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        helpers : {
          title : {
            type : 'over'
          }
        }
      });

      // Set custom style, close if clicked, change title type and overlay color
      $(".fancybox-effects-c").fancybox({
        wrapCSS    : 'fancybox-custom',
        closeClick : true,

        openEffect : 'none',

        helpers : {
          title : {
            type : 'inside'
          },
          overlay : {
            css : {
              'background' : 'rgba(238,238,238,0.85)'
            }
          }
        }
      });

      // Remove padding, set opening and closing animations, close if clicked and disable overlay
      $(".fancybox-effects-d").fancybox({
        padding: 0,

        openEffect : 'elastic',
        openSpeed  : 150,

        closeEffect : 'elastic',
        closeSpeed  : 150,

        closeClick : true,

        helpers : {
          overlay : null
        }
      });

      /*
       *  Button helper. Disable animations, hide close button, change title type and content
       */

      $('.fancybox-buttons').fancybox({
        openEffect  : 'none',
        closeEffect : 'none',

        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : false,

        helpers : {
          title : {
            type : 'inside'
          },
          buttons : {}
        },

        afterLoad : function() {
          this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
        }
      });


      /*
       *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
       */

      $('.fancybox-thumbs').fancybox({
        prevEffect : 'none',
        nextEffect : 'none',

        closeBtn  : false,
        arrows    : false,
        nextClick : true,

        helpers : {
          thumbs : {
            width  : 50,
            height : 50
          }
        }
      });

      /*
       *  Media helper. Group items, disable animations, hide arrows, enable media and button helpers.
      */
      $('.fancybox-media')
        .attr('rel', 'media-gallery')
        .fancybox({
          openEffect : 'none',
          closeEffect : 'none',
          prevEffect : 'none',
          nextEffect : 'none',

          arrows : false,
          helpers : {
            media : {},
            buttons : {}
          }
        });

      /*
       *  Open manually
       */

      $("#fancybox-manual-a").click(function() {
        $.fancybox.open('1_b.jpg');
      });

      $("#fancybox-manual-b").click(function() {
        $.fancybox.open({
          href : 'iframe.html',
          type : 'iframe',
          padding : 5
        });
      });

      $("#fancybox-manual-c").click(function() {
        $.fancybox.open([
          {
            href : '1_b.jpg',
            title : 'My title'
          }, {
            href : '2_b.jpg',
            title : '2nd title'
          }, {
            href : '3_b.jpg'
          }
        ], {
          helpers : {
            thumbs : {
              width: 75,
              height: 50
            }
          }
        });
      });


    });
  </script>
	<div id="content">
		<div class="container">
      <div class="row">
      <div class="arrow"></div><p class="akapit">wypożyczalnia dla Ciebie!</p>
      <div class="col-xs-10 col-sm-10 col-md-10">
      <p class="tekst_1">Szukasz nowoczesnego samochodu? Potrzebujesz samochodu na kilka dni?
        Nie chcesz przepłacać? Jesteś w dobrym miejscu! Zapoznaj się z naszą ofertą!</p><br>
    </div></div>

    <!--info-->
<?php 
   $id=$_GET['id'];    
   $sql = "SELECT * FROM samochody WHERE id='$id'"; 
 $result=$conn->query($sql);
        while($row = mysqli_fetch_array($result)){
  $id_grupy=$row['id_grupy']; 
   $id_auta=$row['id_auta'];                         
$foto = ("SELECT * FROM `photos` WHERE `id_grupy`='$id_grupy' and hex=1"); 
  $resultt=$conn->query($foto);
  if($roww = mysqli_fetch_array($resultt)){
echo'<div class="col-xs-12 col-sm-6 col-md-6">
  <div class="square_1" style="background-image:url(./admin/upload/'.$roww['url'].');"></a></div></div>';

  }
   $idauta = ("SELECT * FROM `cennik` WHERE `id_auta`='$id_auta'"); 
  $resultt_1=$conn->query($idauta);
  if($roww_1 = mysqli_fetch_array($resultt_1)){
  echo '<div class="col-xs-12 col-sm-6 col-md-6">
  <div class="square_2"><div class="nazwa">'.$row['marka'].' '.$row['model'].'</div><hr class="squer">
    <p class="cena">Ceny od</p><div class="pln">'.$roww_1['cena_3'].' Pln</div><p class="opis">brutto za dzień</p>
    <div class="dane_techniczne">Kolor:<b>'.$row['kolor'].'</b> <br> Dostępnych sztuk:<b>1</b> <br> skrzynia: <b>'.$row['gear'].'</b>, Rocznik <b>'.$row['rok_produkcji'].'</b></div><div class="cennik"><hr><p class="cennikk">CENNIK</p>';
   
    echo'<p class="promocje_cennik"><left>1 - 7 DNI <span> '.$roww_1['cena_promocyjna'].' PLN / DOBA</span></p>
    <p class="promocje_cennik"><left>8 - 14 DNI <span> '.$roww_1['cena_1'].' PLN / DOBA</span></p>
   <p class="promocje_cennik"><left>15 - 30 DNI  <span> '.$roww_1['cena_2'].' PLN / DOBA</span></p>
    </div>
  </div>';
echo'

<table class="table table-bordered tg-yw4l>
  <tr>
  <td class="tg-yw4l"></td>
    <th class="tg-yw4l">1 - 7 DNI</th>
    <th class="tg-yw4l">8 - 14 DNI </th>
    <th class="tg-yw4l">15 - 30 DNI</th>
    <th class="tg-yw4l">Miesiąc</th>
    <th class="tg-yw4l">Kaucja</td>
  </tr>
  <tr>
    <td class="tg-yw4l">'.$roww_1['cena_promocyjna'].' PLN</td>
    <td class="tg-yw4l">'.$roww_1['cena_1'].' PLN</td>
    <td class="tg-yw4l">'.$roww_1['cena_2'].' PLN</td>
    <td class="tg-yw4l">'.$roww_1['cena_3'].' PLN</td>
    <td class="tg-yw4l">'.$roww_1['cena_4'].' PLN</td>
  </tr>
</table></div>';
}
 $foto_galeria = ("SELECT * FROM `photos` WHERE `id_grupy`='$id_grupy'"); 
  $result_galeria=$conn->query($foto_galeria);
  while($row_galeria = mysqli_fetch_array($result_galeria)){
    echo '<a class="fancybox" href="./admin/upload/'.$row_galeria['url'].'" data-fancybox-group="gallery" title=""><img src="./admin/upload/'.$row_galeria['url'].'" class="foto"/></a>';
  }
  echo "<hr style='background:grey;'>";
  echo '<div class="row">
  <div class="col-xs-12 col-sm-4 col-md-4">
  <table>
  <tr>
    <td>Silnik:</th><th>'.$row['silnik'].'</td>
  </tr>
  <tr>
    <td>Moc:</td><th>'.$row['moc'].' KM</th>
  </tr>
  <tr>
    <td>Skrzynia biegów:</td><th>'.$row['gear'].'</th>
  </tr>
  <tr>
    <td>Zmiornik paliwa:</td><th>'.$row['bak'].'l</th>
  </tr>
  <tr>
    <td>Spalanie:</th><th>'.$row['zuzycie_paliwa'].' l/100km</td>
  </tr>
   <tr>
    <td>Paliwo:</td><th>'.$row['paliwo'].'</th>
  </tr>
    <tr>
    <td>Liczba drzwi:</td><th>'.$row['miejsca'].'</th>
  </tr>
   <tr>
    <td>Kolor:</td><th style="text-transform:uppercase;">'.$row['kolor'].'</th>
  </tr>
   <tr>
    <td>Tapicerka:</td><th>'.$row['tapicerka'].'</th>
  </tr>
</table>
  </div>
   <div class="col-xs-12 col-sm-4 col-md-4">
        <table>
  <tr>
    <td>Typ:</td><th style="text-transform:uppercase;">'.$row['typ'].'</th>
  </tr>
   <tr>
    <td>Rok produkcji:</td><th>'.$row['rok_produkcji'].'</th>
  </tr>
  <tr>
    <td style="top:0;">Wyposażenie:</td><th>'; echo str_replace(",","<br>",$row['inne']);
    echo'</th>
  </tr>
</table>
  </div>
</div>';
}
 
?>

<br>
       <?php
$j=0;     
   $auto = "SELECT `marka`,`model`,`id_grupy`,`id` FROM `samochody`ORDER BY RAND() LIMIT 3"; 
 $wynik=$conn->query($auto);
        while($row_3 = mysqli_fetch_array($wynik)){

$id_grupy=$row_3['id_grupy'];   

$auto_2 = "SELECT * FROM `photos` WHERE id_grupy='$id_grupy' AND hex=1"; 
 $wynik_2=$conn->query($auto_2);
        while($row_4 = mysqli_fetch_array($wynik_2)){
$j++;
   echo '<div class="col-xs-12 col-sm-4 col-md-4"> <div class="row">
        <div class="baner" style="background-image:url(./admin/upload/'.$row_4['url'].');">';
       if($j==2){ echo'<div class="info2">';
     }else{echo'<div class="info">';}
        echo''.$row_3['marka'].' '.$row_3['model'].' <a href="info.php?id='.$row_3['id'].'"><p class="oferta">sprawdz ofertę</p></a></div></div>
      </div></div>';                  
}
}
$conn->close();
?> 

</div>
		<br><br>
      <br><br>
        <br><br>
        <footer>
            <div id="footer_img">
          <a href="https://web.facebook.com/Wypożyczalnia-w-Kratkę-373370979670559"><img src="./img/facebook-app-logo.png"></a>
          </div>
        </footer>
    </div>

	</div>
</body>

</html>
