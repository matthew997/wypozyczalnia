-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 17 Sty 2017, 17:26
-- Wersja serwera: 10.1.18-MariaDB-cll-lve
-- Wersja PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `wypozyc4_to`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cennik`
--

CREATE TABLE `cennik` (
  `id` int(11) NOT NULL,
  `cena_promocyjna` int(11) NOT NULL,
  `cena_1` int(11) NOT NULL,
  `cena_2` int(11) NOT NULL,
  `cena_3` int(11) NOT NULL,
  `cena_4` int(11) NOT NULL,
  `id_auta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `cennik`
--

INSERT INTO `cennik` (`id`, `cena_promocyjna`, `cena_1`, `cena_2`, `cena_3`, `cena_4`, `id_auta`) VALUES
(1, 75, 70, 65, 55, 300, 531501093),
(4, 175, 165, 155, 145, 600, 901401089),
(2, 135, 130, 125, 115, 500, 471501017),
(3, 115, 105, 95, 85, 400, 661401014),
(6, 115, 105, 95, 85, 400, 503101018);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `kontakt`
--

CREATE TABLE `kontakt` (
  `id` int(11) NOT NULL,
  `ulica` text COLLATE utf8_bin NOT NULL,
  `miasto` text CHARACTER SET utf8 NOT NULL,
  `kod_pocztowy` text COLLATE utf8_bin NOT NULL,
  `adres_email` text COLLATE utf8_bin NOT NULL,
  `adres_email_2` text COLLATE utf8_bin NOT NULL,
  `telefon` int(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `kontakt`
--

INSERT INTO `kontakt` (`id`, `ulica`, `miasto`, `kod_pocztowy`, `adres_email`, `adres_email_2`, `telefon`) VALUES
(1, 'Jutrzenki 96 A', 'Warszawa', '02-230', 'kontakt@wypozyczalniawkratke.pl', 'rozkosz2514@gmail.com', 578234657);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `menu`
--

CREATE TABLE `menu` (
  `id` smallint(6) NOT NULL,
  `nazwa` text COLLATE utf8_bin NOT NULL,
  `url` text COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `menu`
--

INSERT INTO `menu` (`id`, `nazwa`, `url`) VALUES
(8, 'Home', 'http://localhost/park-php/'),
(9, 'Facebook', 'https://www.facebook.com/parklinowyustron/?fref=ts'),
(10, 'Galeria', 'http://localhost/park-php/galeria.php'),
(11, 'Kontakt', './kontakt.php'),
(12, 'Budowa Parku', './budowa_parku.php');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `news`
--

CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `tytul` text COLLATE utf8_bin NOT NULL,
  `tekst` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `data` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `news`
--

INSERT INTO `news` (`id`, `tytul`, `tekst`, `data`) VALUES
(5, 'test1234', '<h1>Tekst nagÅ‚&oacute;wkowy</h1>\r\n\r\n<blockquote>\r\n<p><strong>Tydzie? prawdopodobnie marnie zako?czy si</strong>? dla Samsunga - wieczorem firma powinna og?osi?, ?e klienci musz? zwryoijci? ?wie?o kupione Galaxy Note 7. Fablet, a raczej niewielka cz??? egzemplarzy, ma fabryczny defekt, erfgfuoefwyoewfwf igfoiuewgyfowwe<sub>x</sub></p>\r\n</blockquote>\r\n', '2016-09-02'),
(7, 'Nowy wpis na blogu', '<p>Ä„Ä†Ä˜&Oacute;Åƒ&Aacute;LÅMNOPRSÅš&nbsp;</p>\r\n\r\n<div>\r\n<p><iframe frameborder="0" height="315" src="https://www.youtube.com/embed/iXVxweWXv4o" width="560"></iframe></p>\r\n</div>\r\n', '2016-09-02'),
(9, 'AktualnoÅ›ci', '<p><iframe frameborder="0" height="400" src="https://www.youtube.com/embed/iXVxweWXv4o" width="100%"></iframe></p>\r\n\r\n<p style="text-align:center">Szanowni PaÅ„stwo!<br />\r\n<span style="color:#ff0000">Dzisiaj, tj. 06.09.2016, ze wzglÄ™du na zÅ‚Ä… pogodÄ™ park linowy bÄ™dzie zamkniÄ™ty.</span><br />\r\n<br />\r\nWe wrzeÅ›niu park linowy otwarty jest w kaÅ¼dy pogodny dzieÅ„. Od poniedziaÅ‚ku do piÄ…tku od godz. 11.00 do godz. 18.00, w soboty i niedziele od godz. 10.00 - 18.00. Serdecznie zapraszamy!!!<br />\r\nIstnieje moÅ¼liwoÅ›Ä‡ wczeÅ›niejszego otwarcia parku dla grup od 15 os&oacute;b. Prosimy o wczeÅ›niejszy kontakt telefoniczny.<br />\r\nWÅ‚aÅ›ciciel zastrzega sobie moÅ¼liwoÅ›Ä‡ zamkniÄ™cia parku w przypadku zÅ‚ych warunk&oacute;w atmosferycznych.<br />\r\n<br />\r\nSerdecznie zapraszamy!!!<br />\r\n<br />\r\nPoniÅ¼ej link do kamery:<br />\r\nhttp://oognet.pl/content/details/1287<br />\r\n<br />\r\nZOBACZ NOWE ZDJÄ˜CIA PARKU NA www.facebook.com/parklinowyustron</p>\r\n', '2016-09-06');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `o_nas`
--

CREATE TABLE `o_nas` (
  `id` int(11) NOT NULL,
  `o_nas` longtext COLLATE utf8_unicode_ci NOT NULL,
  `uslugi` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Zrzut danych tabeli `o_nas`
--

INSERT INTO `o_nas` (`id`, `o_nas`, `uslugi`) VALUES
(1, '<p>WypoÅ¼yczalnia w kratkÄ™ to nowy koncept powstajÄ…cy w Warszawie przez ludzi, kt&oacute;rzy majÄ… sÅ‚aboÅ›Ä‡ do dalekich podr&oacute;Å¼y i dobrych, ekonomicznych aut. Nasza oferta skierowana jest do os&oacute;b i firm&hellip;</p>\r\n\r\n<p>Bez wzglÄ™du na to, czy potrzebujesz auta ekonomicznego, dziÄ™ki kt&oacute;remu z Å‚atwoÅ›ciÄ… bÄ™dziesz poruszaÄ‡ siÄ™ po miejskich uliczkach, czy komfortowego, idealnego na dÅ‚uÅ¼sze trasy, u nas znajdziesz to, czego szukasz.</p>\r\n\r\n<p>Ponad to masz okazjÄ™ nie tylko korzystaÄ‡ z komfortowych pojazd&oacute;w. JeÅ›li tylko chcesz moÅ¼esz zam&oacute;wiÄ‡ auto wprost pod drzwi biura na parking hotelowy, lotnisko lub w jakiekolwiek wskazane przez Ciebie miejsce. Nasze pojazdy podstawimy zawsze tam, gdzie tego potrzebujesz.</p>\r\n\r\n<p>Nasza oferta skierowana jest do firm i os&oacute;b fizycznych oczekujÄ…cych obsÅ‚ugi na najwyÅ¼szym poziomie , kt&oacute;re w szybki i profesjonalny spos&oacute;b chcÄ… pozyskaÄ‡ pojazd. Wszystkie wynajmowane pojazdy objÄ™te sÄ… gwarancjÄ… producenta oraz peÅ‚nym ubezpieczeniem OC , AC , NNW, ASSISTANCE VIP . Nasza wypoÅ¼yczalnia zapewnia auta zastÄ™pcze w przypadku awarii , kolizji lub innych zdarzeÅ„ losowych.</p>\r\n', '<p><strong>-!WYNAJEM AUTA Z KIEROWCÄ„ POD SAME DRZWI</strong></p>\r\n\r\n<p>UsÅ‚uga door-to-door to dostawa (lub odbi&oacute;r) pojazdu w wyznaczone miejsce, w um&oacute;wionym czasie. Z opcji tej moÅ¼na korzystaÄ‡ r&oacute;wnieÅ¼ przy realizacji przeglÄ…d&oacute;w okresowych lub sezonowej wymiany opon.</p>\r\n\r\n<p>-!<strong>Relokacja pojazdu</strong></p>\r\n\r\n<p>UsÅ‚uga ta polega na przewoÅ¼eniu samochodu z miejsca postoju czy pobytu do miejsca wskazanego przez klienta. W wielu przypadkach, zdarza siÄ™ iÅ¼ zostawiamy samoch&oacute;d w danym miejscu nie majÄ…c p&oacute;Åºniej moÅ¼liwoÅ›ci bÄ…dz teÅ¼ czasu na jego odbi&oacute;r. StÄ…d teÅ¼ nasza inicjatywa w tym kierunku, kt&oacute;rÄ… wykonamy za PaÅ„stwa. SkorzystaÄ‡ z niej moÅ¼e kaÅ¼dy lecz nie ukrywamy, Å¼e naszym docelowym klientem sÄ… firmy, kt&oacute;re posiadajÄ… duÅ¼a flote samochod&oacute;w poruszajÄ…cych siÄ™ po obszarze caÅ‚ego kraju.</p>\r\n\r\n<p>-!<strong>Przew&oacute;z os&oacute;b</strong></p>\r\n\r\n<p>Dolcar zajmujÄ™ siÄ™ r&oacute;wnieÅ¼ przewozem os&oacute;b. Oferujemy przew&oacute;z w kaÅ¼de wyznaczone przez PaÅ„stwa miejsce na terenie caÅ‚ego kraju. Przew&oacute;z odbywa siÄ™ w bardzo komfortowych i wygodnych warunkach od jednej do dziewiÄ™ciu os&oacute;b. UsÅ‚uga obejmuje przew&oacute;z samochodami osobowymi oraz busami ( kierowca + 8 os&oacute;b ). CzÄ™sto tego typu usÅ‚uga jest wykorzystywana w ramach obsÅ‚ugi imprez, wycieczek, wyjazd&oacute;w firmowych oraz konferencji.</p>\r\n\r\n<p>-!<strong>Transport towar&oacute;w</strong></p>\r\n\r\n<p>Mimo wiÄ™kszej czÄ™Å›ci Å›wiadczonych usÅ‚ug przez naszÄ… firmÄ™, kt&oacute;ra skierowana jest gÅ‚&oacute;wnie do os&oacute;b prywatynych, to posiadamy usÅ‚ugi, kt&oacute;re sÄ… skierowane gÅ‚&oacute;wnie dla firm i przedsiÄ™biorc&oacute;w. JednÄ… z tego typu usÅ‚ug jest &quot;Transport towar&oacute;w&quot;, kt&oacute;ra zapewnia szybkie i bezpieczne dostarczenie wszelkiego rodzaju towaru na wskazane miejsce przez klienta na terenie caÅ‚ego kraju. UsÅ‚uga jest wykonywana najczÄ™sciÄ™j samochodami z Grupy T, ze wzglÄ™du na ich pojemnoÅ›Ä‡ wzglÄ™dem towaru.</p>\r\n\r\n<p>-!<strong>Auto z OC sprawcy</strong></p>\r\n\r\n<p>KaÅ¼dy kierowca ma Å›wiadomoÅ›Ä‡ Å¼e codziennie na drodze zdarzajÄ… siÄ™ stÅ‚uczki i wypadki. Niejednokrotnie bywa tak, Å¼e to nie my jesteÅ›my winni w przypadku stÅ‚uczki czy wypadku. W przypadku kiedy szkoda nie powstaÅ‚a z Naszej winy, likwidowana bÄ™dzie z OC sprawcy. W tym wypadku policja powinna spisaÄ‡ oÅ›wiadczenie na miejscu zdarzenia, kt&oacute;re to bÄ™dzie kluczowym dokumentem na uzyskanie samochodu zastÄ™pczego. W przypadku gdy do wykonywania codziennych obowiÄ…zk&oacute;w, potrzebnÄ™ jest nam samoch&oacute;d zastÄ™pczy, Dolcar oferuje sfinalizowanie koszt&oacute;w wynajmu takiego samochodu z ubezpieczalniÄ… i podstawi je w wybrane miejsce przez klienta.</p>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `photos`
--

CREATE TABLE `photos` (
  `id` int(10) NOT NULL,
  `url` text COLLATE utf8_bin NOT NULL,
  `id_grupy` int(11) NOT NULL,
  `hex` int(1) NOT NULL DEFAULT '0',
  `data` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `photos`
--

INSERT INTO `photos` (`id`, `url`, `id_grupy`, `hex`, `data`) VALUES
(175, '1479051612_86316_0.jpg', 70284, 1, '2016-11-13'),
(124, '1477598323_28522_0.jpg', 86124, 1, '2016-10-27'),
(125, '1477598491_31078_0.jpg', 47234, 1, '2016-10-27'),
(132, '1478434713_62048_0.jpg', 24189, 1, '2016-11-06'),
(140, '1478975441_37183_0.png', 42275, 0, '2016-11-12'),
(134, '1478777988_42275_1.jpg', 42275, 0, '2016-11-10'),
(135, '1478777988_42275_2.jpg', 42275, 0, '2016-11-10'),
(136, '1478777988_42275_3.jpg', 42275, 0, '2016-11-10'),
(137, '1478777988_42275_4.jpg', 42275, 0, '2016-11-10'),
(141, '1478975467_57800_0.png', 42275, 0, '2016-11-12'),
(142, '1478975575_99679_0.png', 42275, 0, '2016-11-12'),
(164, '1479043116_43233_3.png', 43233, 0, '2016-11-13'),
(163, '1479043116_43233_2.png', 43233, 0, '2016-11-13'),
(162, '1479043116_43233_1.png', 43233, 0, '2016-11-13'),
(166, '1479043116_43233_5.png', 43233, 0, '2016-11-13'),
(198, '1480697446_72794_0.jpg', 2879, 2, '2016-12-02'),
(197, '1480696659_2879_0.jpg', 2879, 2, '2016-12-02');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `samochody`
--

CREATE TABLE `samochody` (
  `id` int(11) NOT NULL,
  `marka` varchar(11) COLLATE utf8_bin NOT NULL,
  `model` varchar(11) COLLATE utf8_bin NOT NULL,
  `rok_produkcji` int(4) NOT NULL,
  `silnik` varchar(200) COLLATE utf8_bin NOT NULL,
  `moc` int(11) NOT NULL,
  `miejsca` varchar(10) COLLATE utf8_bin NOT NULL,
  `zuzycie_paliwa` varchar(50) COLLATE utf8_bin NOT NULL,
  `bak` int(11) NOT NULL,
  `tapicerka` varchar(250) COLLATE utf8_bin NOT NULL,
  `inne` mediumtext COLLATE utf8_bin NOT NULL,
  `paliwo` varchar(100) COLLATE utf8_bin NOT NULL,
  `typ` varchar(50) COLLATE utf8_bin NOT NULL,
  `kolor` varchar(50) COLLATE utf8_bin NOT NULL,
  `gear` varchar(50) COLLATE utf8_bin NOT NULL,
  `id_grupy` int(11) NOT NULL,
  `id_auta` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Zrzut danych tabeli `samochody`
--

INSERT INTO `samochody` (`id`, `marka`, `model`, `rok_produkcji`, `silnik`, `moc`, `miejsca`, `zuzycie_paliwa`, `bak`, `tapicerka`, `inne`, `paliwo`, `typ`, `kolor`, `gear`, `id_grupy`, `id_auta`) VALUES
(2, 'Skoda', 'Superb', 2015, '1.4 TSI', 125, '5', '5,4', 45, 'MateriaÅ‚owa', 'ÅšwiatÅ‚a LED, Funkcja START-STOP, Tempomat, Dwustrefowa klimatyzacja', 'diesel', 'sedan', 'BiaÅ‚y', 'Manualna', 47234, 901401089),
(4, 'Skoda', 'Octavia', 2013, '1.0 TSI', 115, '5', '4.5', 45, 'MateriaÅ‚owa', 'WyÅ›wietlacz MAXI-Dot, Czujniki parkowania, ÅšwiatÅ‚a przeciwmgÅ‚owe, Komputer pokÅ‚adowy', 'Benzyna', 'sedan', 'BiaÅ‚y', 'Manualna', 86124, 471501017),
(8, 'Skoda ', 'Citigo', 2015, '1.0', 60, '5', '5.5', 55, 'MateriaÅ‚owa', 'ABS ESP, KLIMATYZACJA, ELEKTRYCZNE SZYBY, RADIO CD / MP3, KOMPUTER POKÅADOWY', 'Benzyna', 'station-wagon', 'biaÅ‚y', 'Manualna', 70284, 531501093),
(12, 'Skoda', 'Rapid', 2017, '1.2 TSI', 90, '5', '4,7', 45, 'MateriaÅ‚owa', 'ESP z ABS, KLIMATYZACJA,ELEKTRYCZNE SZYBY,RADIO BLUES(USB,SD,AUX-IN), KOMPUTER POKÅADOWY', 'diesel', 'sedan', 'BiaÅ‚y', 'Manualna', 24189, 503101018);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `user`
--

CREATE TABLE `user` (
  `id_user` int(6) NOT NULL,
  `login` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_polish_ci NOT NULL,
  `mail` varchar(255) COLLATE utf8_polish_ci NOT NULL,
  `admin` varchar(1) COLLATE utf8_polish_ci NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `user`
--

INSERT INTO `user` (`id_user`, `login`, `password`, `mail`, `admin`) VALUES
(3, 'admin', 'e3bc38a4faa625d074664d572d810c1e', 'rozkosz2514@gmail.com', '1'),
(36, 'bestszeller', 'f012f52244251f36e2ae11142c132467', 'bestszeller@gmail.com', '1'),
(39, 'bartek', 'c98c1fdb9054218ab27ff2da15a50d62', 'bartekdrozdzik97@gmail.com', '1');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `cennik`
--
ALTER TABLE `cennik`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_2` (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `samochody`
--
ALTER TABLE `samochody`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cennik`
--
ALTER TABLE `cennik`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT dla tabeli `menu`
--
ALTER TABLE `menu`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT dla tabeli `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT dla tabeli `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT dla tabeli `samochody`
--
ALTER TABLE `samochody`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT dla tabeli `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
